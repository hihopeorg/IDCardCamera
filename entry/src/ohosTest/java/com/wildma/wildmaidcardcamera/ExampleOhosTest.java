package com.wildma.wildmaidcardcamera;

import com.wildma.idcardcamera.cropper.CropOverlayView;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.Size;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {

    CropOverlayView mCropOverlayView;
    private Context context;

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.wildma.wildmaidcardcamera", actualBundleName);
    }

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mCropOverlayView = new CropOverlayView(context);
    }

    @Test
    public void setup_1() {
        Assert.assertEquals("It is ok",mCropOverlayView.createEmptyPixelMap(null),null);
    }

    @Test
    public void setup_2() {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        mCropOverlayView.setComponentSize(80,80);
        initializationOptions.size = new Size(50, 50);
        initializationOptions.alphaType = AlphaType.UNKNOWN;
        PixelMap pixelMap = PixelMap.create(initializationOptions);
        Assert.assertEquals("It is ok",mCropOverlayView.createEmptyPixelMap(pixelMap),null);
    }

}