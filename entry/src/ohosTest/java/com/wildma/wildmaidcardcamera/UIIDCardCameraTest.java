/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wildma.wildmaidcardcamera;


import com.wildma.idcardcamera.camera.CameraAbility;
import junit.framework.TestCase;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.Assert;

/**
 * 注意  务必真机测试，并且手动授权危险权限
 */
public class UIIDCardCameraTest extends TestCase {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;

    public void setUp() throws Exception {
        super.setUp();
        mContext = sAbilityDelegator.getAppContext();
    }

    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        sleep(3);
    }

    public void testFrontAbility() {
        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        AbilitySlice abilitySlice = sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        sleep(5);
        Text mStatus = (Text) mainAbility.findComponentById(ResourceTable.Id_front);
        System.out.println("zjj mStatus"+mStatus.getText());
        Assert.assertEquals(mStatus.getText(),"身份证正面");
    }

    public void testBackAbility() {
        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        AbilitySlice abilitySlice = sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        sleep(5);
        Text mStatus = (Text) mainAbility.findComponentById(ResourceTable.Id_back);
        Assert.assertEquals(mStatus.getText(),"身份证反面");
        sleep(1);
    }

    public void testCameraAbility() {
        Ability mainAbility = EventHelper.startAbility(CameraAbility.class);
        sleep(5);
        Image takeSingleCapture = (Image) mainAbility.findComponentById(ResourceTable.Id_iv_camera_take);
        EventHelper.triggerClickEvent(mainAbility,takeSingleCapture);
        sleep(1);
    }

    private void sleep(int duration) {
        try {
            Thread.sleep(duration * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}