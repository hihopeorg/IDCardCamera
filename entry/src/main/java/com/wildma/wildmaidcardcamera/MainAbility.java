/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wildma.wildmaidcardcamera;

import com.huawei.agconnect.common.TextUtils;
import com.wildma.idcardcamera.camera.IDCardCamera;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;


/**
 * MainAbility
 */
public class MainAbility extends Ability implements Component.ClickedListener {
    private Button mFront;
    private Button mBack;
    private Image mIvFront;
    private Image mIvBack;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mFront = (Button) findComponentById(ResourceTable.Id_front);
        mBack = (Button) findComponentById(ResourceTable.Id_back);
        mIvFront = (Image) findComponentById(ResourceTable.Id_iv_front);
        mIvBack = (Image) findComponentById(ResourceTable.Id_iv_back);
        mFront.setClickedListener(this);
        mBack.setClickedListener(this);
    }

    public void front(Component component) {
        IDCardCamera.create(this).openCamera(IDCardCamera.TYPE_IDCARD_FRONT);
    }

    public void back(Component component) {
        IDCardCamera.create(this).openCamera(IDCardCamera.TYPE_IDCARD_BACK);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultCode == IDCardCamera.RESULT_CODE) {
            final String path = IDCardCamera.getImagePath(resultData);
            if (!TextUtils.isEmpty(path)) {
                PixelMap pixelMap = decodeFile(path);
                if (requestCode == IDCardCamera.TYPE_IDCARD_FRONT) {
                    mIvFront.setPixelMap(pixelMap);
                } else if (requestCode == IDCardCamera.TYPE_IDCARD_BACK) {
                    mIvBack.setPixelMap(pixelMap);
                }
            }
        }
    }

    private PixelMap decodeFile(String path) {
        PixelMap pixelMap = null;
        final Uri uri = Uri.parse(path);
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        try {
            FileDescriptor fd = helper.openFile(uri, "r");
            ImageSource imageSource = ImageSource.create(fd, null);
            pixelMap = imageSource.createPixelmap(null);
        } catch (
                DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }
        return pixelMap;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_front:
                front(component);
                break;
            case ResourceTable.Id_back:
                back(component);
                break;
            default:
                break;
        }
    }
}
