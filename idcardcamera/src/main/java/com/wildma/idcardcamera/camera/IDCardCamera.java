package com.wildma.idcardcamera.camera;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;

import java.lang.ref.WeakReference;

/**
 * Author       wildma
 * Github       https://github.com/wildma
 * Date         2019/04/28
 * Desc	        ${身份证相机}
 */
public class IDCardCamera {

    public final static int    TYPE_IDCARD_FRONT     = 1;//身份证正面
    public final static int    TYPE_IDCARD_BACK      = 2;//身份证反面
    public final static int    RESULT_CODE           = 0X11;//结果码
    public final static int    PERMISSION_CODE_FIRST = 0x12;//权限请求码
    public final static String TAKE_TYPE             = "take_type";//拍摄类型标记
    public final static String IMAGE_PATH            = "image_path";//图片路径标记
    public Context mContext;

    private final WeakReference<Ability> mAbility;
    private final WeakReference<Fraction> mFraction;

    public static IDCardCamera create(Ability Ability) {
        return new IDCardCamera(Ability);
    }

    public static IDCardCamera create(Fraction Fraction) {
        return new IDCardCamera(Fraction);
    }

    private IDCardCamera(Ability Ability) {
        this(Ability, (Fraction) null);
    }

    private IDCardCamera(Fraction Fraction) {
        this(Fraction.getFractionAbility(), Fraction);
    }

    private IDCardCamera(Ability ability, Fraction fraction) {
        this.mAbility = new WeakReference(ability);
        this.mFraction = new WeakReference(fraction);
        if (fraction != null) {
            mContext = fraction;
        } else {
            mContext = ability;
        }
    }

    /**
     * 打开相机
     *
     * @param IDCardDirection 身份证方向（TYPE_IDCARD_FRONT / TYPE_IDCARD_BACK）
     */
    public void openCamera(int IDCardDirection) {
        Ability activity = this.mAbility.get();
        Fraction fragment = this.mFraction.get();
        Intent intent = new Intent();
        intent.setParam(TAKE_TYPE, IDCardDirection);
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(mContext.getBundleName())
                .withAbilityName(CameraAbility.class.getName())
                .build();
        intent.setOperation(operation);
        if (fragment != null) {
            //not support
            //fragment.startAbilityForResult(intent, IDCardDirection);
        } else {
            activity.startAbilityForResult(intent, IDCardDirection);
        }
    }

    /**
     * 获取图片路径
     *
     * @param data Intent
     * @return 图片路径
     */
    public static String getImagePath(Intent data) {
        if (data != null) {
            return data.getUriString();
        }
        return "";
    }
}

