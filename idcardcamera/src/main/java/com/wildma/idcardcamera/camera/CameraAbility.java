/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wildma.idcardcamera.camera;


import com.wildma.idcardcamera.ResourceTable;
import com.wildma.idcardcamera.cropper.CropImageView;
import com.wildma.idcardcamera.cropper.CropListener;
import com.wildma.idcardcamera.cropper.CropOverlayView;
import com.wildma.idcardcamera.utils.FileUtils;
import com.wildma.idcardcamera.utils.ImageUtils;
import com.wildma.idcardcamera.utils.ScreenUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.render.opengl.Utils;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.data.rdb.ValuesBucket;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.camera.CameraKit;
import ohos.media.camera.device.*;
import ohos.media.camera.params.AfResult;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageReceiver;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageFormat;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.security.SystemPermission;
import ohos.utils.net.Uri;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;
import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_PICTURE;
import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_PREVIEW;
import static ohos.media.camera.params.Metadata.FlashMode.FLASH_ALWAYS_OPEN;
import static ohos.media.camera.params.Metadata.FlashMode.FLASH_CLOSE;

/**
 * TakePhotoAbility
 */
public class CameraAbility extends Ability implements ImageReceiver.IImageArrivalListener {
    private static final String TAG = CameraAbility.class.getSimpleName();

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, TAG);

    private static final int SCREEN_WIDTH = 1080;

    private static final int SCREEN_HEIGHT = 1920;

    private static final int IMAGE_RCV_CAPACITY = 9;

    private SurfaceProvider surfaceProvider;

    private ImageReceiver imageReceiver;

    private boolean isFrontCamera = false;

    private Surface previewSurface;

    private Camera cameraDevice;

    private Component buttonGroupLayout;

    private DirectionalLayout surfaceContainer;

    private String cameraId = "";
    private CameraKit cameraKit;
    private boolean isOpen = false;
    private CropImageView mCropImageView;
    private Component mLlCameraCropContainer;
    private Image mIvCameraCrop;
    private Image mIvCameraFlash;
    private Component mLlCameraOption;
    private Component mLlCameraResult;
    private Text mViewCameraCropBottom;
    private StackLayout mFlCameraOption;
    private Component mViewCameraCropLeft;
    private Component mViewCameraCropTop;
    private FrameConfig.Builder framePreviewConfigBuilder;
    private ohos.media.camera.device.CameraAbility cameraAbility;
    private Context mContext;
    private int offsetX = 0;
    private int offsetY = 0;
    private int mType;//拍摄类型
    private boolean isToast = true;//是否弹吐司，为了保证for循环只弹一次
    private String[] permissions = {
            SystemPermission.WRITE_USER_STORAGE, SystemPermission.READ_USER_STORAGE, SystemPermission.CAMERA,
            SystemPermission.MICROPHONE, SystemPermission.LOCATION
    };

    private final EventHandler eventHandler = new EventHandler(EventRunner.current()) {
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_camera);
        mContext = this;
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);//隐藏状态栏
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);//沉浸式状态栏
        mType = getIntent().getIntParam(IDCardCamera.TAKE_TYPE, 1);
    }

    @Override
    protected void onActive() {
        super.onActive();
        checkReadPermission();
    }

    /**
     * This method is used to check permission
     */
    public void checkReadPermission() {
        List<String> permissions = new LinkedList<>(Arrays.asList(SystemPermission.WRITE_USER_STORAGE, SystemPermission.READ_USER_STORAGE, SystemPermission.CAMERA,
                SystemPermission.MICROPHONE, SystemPermission.LOCATION));
        permissions.removeIf(
                permission -> verifySelfPermission(permission) == PERMISSION_GRANTED || !canRequestPermission(permission));

        if (!permissions.isEmpty()) {
            requestPermissionsFromUser(permissions.toArray(new String[permissions.size()]), IDCardCamera.PERMISSION_CODE_FIRST);
        } else {
            initComponents();
            initSurface();
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions == null || permissions.length == 0 || grantResults == null || grantResults.length == 0) {
            return;
        }
        for (int grantResult : grantResults) {
            if (grantResult != IBundleManager.PERMISSION_GRANTED) {
                terminateAbility();
                break;
            }
        }
    }

    private void initSurface() {
        getWindow().setTransparent(true);
        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        surfaceProvider = new SurfaceProvider(this);
        surfaceProvider.setLayoutConfig(params);
        surfaceProvider.pinToZTop(false);
        if (surfaceProvider.getSurfaceOps().isPresent()) {
            surfaceProvider.getSurfaceOps().get().addCallback(new SurfaceCallBack());
        }
        surfaceContainer.addComponent(surfaceProvider);
    }


    private void initComponents() {
        surfaceContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_camera_preview);
        mLlCameraCropContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_ll_camera_crop_container);
        mIvCameraCrop = (Image) findComponentById(ResourceTable.Id_iv_camera_crop);
        mIvCameraFlash = (Image) findComponentById(ResourceTable.Id_iv_camera_flash);
        mLlCameraOption = (DirectionalLayout) findComponentById(ResourceTable.Id_ll_camera_option);
        mLlCameraResult = (DirectionalLayout) findComponentById(ResourceTable.Id_ll_camera_result);
        mCropImageView = (CropImageView) findComponentById(ResourceTable.Id_crop_image_view);
        mViewCameraCropBottom = (Text) findComponentById(ResourceTable.Id_view_camera_crop_bottom);
        mFlCameraOption = (StackLayout) findComponentById(ResourceTable.Id_fl_camera_option);
        mViewCameraCropLeft = findComponentById(ResourceTable.Id_view_camera_crop_left);
        mViewCameraCropTop = findComponentById(ResourceTable.Id_view_camera_crop_top);

        float screenMinSize = Math.min(ScreenUtils.getScreenWidth(this), ScreenUtils.getScreenHeight(this));
        float screenMaxSize = Math.max(ScreenUtils.getScreenWidth(this), ScreenUtils.getScreenHeight(this));
        float height = (int) (screenMinSize * 0.75);
        float width = (int) (height * 75.0f / 47.0f);
        //获取底部"操作区域"的宽度
        offsetX = (int) (screenMaxSize - width) / 2;
        offsetY = (int) (screenMinSize - height) / 2;
        DirectionalLayout.LayoutConfig containerParams = new DirectionalLayout.LayoutConfig((int) width, DirectionalLayout.LayoutConfig.MATCH_PARENT);
        DirectionalLayout.LayoutConfig cropParams = new DirectionalLayout.LayoutConfig((int) width, (int) height);
        DirectionalLayout.LayoutConfig cameraOptionParams = new DirectionalLayout.LayoutConfig((int) offsetX, DirectionalLayout.LayoutConfig.MATCH_PARENT);
        mLlCameraCropContainer.setLayoutConfig(containerParams);
        mIvCameraCrop.setLayoutConfig(cropParams);
        //获取"相机裁剪区域"的宽度来动态设置底部"操作区域"的宽度，使"相机裁剪区域"居中
        mFlCameraOption.setLayoutConfig(cameraOptionParams);
        switch (mType) {
            case IDCardCamera.TYPE_IDCARD_FRONT:
                mIvCameraCrop.setPixelMap(ResourceTable.Media_camera_idcard_front);
                break;
            case IDCardCamera.TYPE_IDCARD_BACK:
                mIvCameraCrop.setPixelMap(ResourceTable.Media_camera_idcard_back);
                break;
        }
        mViewCameraCropBottom.setClickedListener(this::setAutoFocus);
        findComponentById(ResourceTable.Id_iv_camera_take).setClickedListener(this::takeSingleCapture);
        findComponentById(ResourceTable.Id_iv_camera_close).setClickedListener(this::terminate);
        findComponentById(ResourceTable.Id_iv_camera_flash).setClickedListener(this::takeFlash);
        findComponentById(ResourceTable.Id_iv_camera_result_ok).setClickedListener(this::confirm);
        findComponentById(ResourceTable.Id_iv_camera_result_cancel).setClickedListener(this::setTakePhotoLayout);
    }

    private void terminate(Component component) {
        terminateAbility();
    }

    private void openCamera() {
        imageReceiver = ImageReceiver.create(SCREEN_WIDTH, SCREEN_HEIGHT, ImageFormat.JPEG, IMAGE_RCV_CAPACITY);
        imageReceiver.setImageArrivalListener(this);
        cameraKit = CameraKit.getInstance(getApplicationContext());
        String[] cameraList = cameraKit.getCameraIds();
        for (String logicalCameraId : cameraList) {
            int faceType = cameraKit.getCameraInfo(logicalCameraId).getFacingType();
            switch (faceType) {
                case CameraInfo.FacingType.CAMERA_FACING_FRONT:
                    if (isFrontCamera) {
                        cameraId = logicalCameraId;
                    }
                    break;
                case CameraInfo.FacingType.CAMERA_FACING_BACK:
                    if (!isFrontCamera) {
                        cameraId = logicalCameraId;
                    }
                    break;
                case CameraInfo.FacingType.CAMERA_FACING_OTHERS:
                default:
                    break;
            }
        }
        if (cameraId != null && !cameraId.isEmpty()) {
            CameraStateCallbackImpl cameraStateCallback = new CameraStateCallbackImpl();
            cameraKit.createCamera(cameraId, cameraStateCallback, eventHandler);
        }
        cameraAbility = cameraKit.getCameraAbility(cameraId);
    }

    public void takeSingleCapture(Component component) {
        if (cameraDevice == null || imageReceiver == null) {
            return;
        }
        FrameConfig.Builder framePictureConfigBuilder = cameraDevice.getFrameConfigBuilder(FRAME_CONFIG_PICTURE);
        framePictureConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
        FrameConfig pictureFrameConfig = framePictureConfigBuilder.build();
        cameraDevice.triggerSingleCapture(pictureFrameConfig);
    }

    public void takeFlash(Component component) {
        if (!isOpen) {
            isOpen = true;
            openFlashlight();
        } else {
            isOpen = false;
            closeFlashlight();
        }
        mIvCameraFlash.setPixelMap(isOpen ? ResourceTable.Media_camera_flash_on : ResourceTable.Media_camera_flash_off);
    }

    void openFlashlight() {
        if (cameraDevice != null) {
            framePreviewConfigBuilder.setFlashMode(FLASH_ALWAYS_OPEN);
            cameraDevice.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }
    }

    void closeFlashlight() {
        if (cameraDevice != null) {
            framePreviewConfigBuilder.setFlashMode(FLASH_CLOSE);
            cameraDevice.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }
    }

    /**
     * 裁剪图片
     */
    public void cropImage(PixelMap bitmap) {
        /*计算扫描框的坐标点*/
        float left = mIvCameraCrop.getEstimatedWidth();
        float top = mIvCameraCrop.getTop();
        float right = mIvCameraCrop.getRight();
        float bottom = mIvCameraCrop.getBottom();
        /*计算扫描框坐标点占原图坐标点的比例*/
        float leftProportion = left / surfaceContainer.getEstimatedWidth();
        float topProportion = top / surfaceContainer.getEstimatedHeight();
        float rightProportion = right / surfaceContainer.getEstimatedWidth();
        float bottomProportion = bottom / surfaceContainer.getBottom();
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.size = bitmap.getImageInfo().size;
        options.size.width = bitmap.getImageInfo().size.width;
        options.size.height = bitmap.getImageInfo().size.height;

        /*设置成手动裁剪模式*/
        getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                //mIvPP.setPixelMap(mCropBitmap);
                //将手动裁剪区域设置成与扫描框一样大
                mCropImageView.setComponentSize(mIvCameraCrop.getEstimatedWidth(), mIvCameraCrop.getEstimatedHeight());
                setCropLayout();
                mIvCameraCrop.setPixelMap(bitmap);
                mCropImageView.setImageBitmap(bitmap);
            }
        });
    }

    /**
     * 点击确认，返回图片路径
     */
    private void confirm(Component component) {
        /*手动裁剪图片*/
        mCropImageView.crop(new CropListener() {
            @Override
            public void onFinish(PixelMap bitmap) {
                {
                    if (bitmap == null) {
                        ToastDialog toastDialog = new ToastDialog(mContext);
                        toastDialog.setText(getString(ResourceTable.String_crop_fail)).setDuration(1000).show();
                        terminateAbility();
                    }

                    /*保存图片到sdcard并返回图片路径*/
                    Uri uri = saveImage(bitmap);
                    Intent intent = new Intent();
                    intent.setUri(uri);
                    setResult(IDCardCamera.RESULT_CODE, intent);
                    terminateAbility();
                }
            }
        }, true);
    }

    private String mCompressFormat = "jpeg";

    private Uri saveImage(PixelMap pixelMap) {
        String fileName = new StringBuffer().append(System.currentTimeMillis()).append(".jpg").toString();
        try {
            ValuesBucket valuesBucket = new ValuesBucket();
            valuesBucket.putString(AVStorage.Images.Media.DISPLAY_NAME, fileName);
            valuesBucket.putString("relative_path", "DCIM/");
            valuesBucket.putString(AVStorage.Images.Media.MIME_TYPE, "image/" + mCompressFormat);
            //应用独占
            valuesBucket.putInteger("is_pending", 1);
            DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
            int id = helper.insert(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, valuesBucket);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
            //这里需要"w"写权限
            FileDescriptor fd = helper.openFile(uri, "w");
            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            OutputStream outputStream = new FileOutputStream(fd);
            packingOptions.quality = 90;
            boolean result = imagePacker.initializePacking(outputStream, packingOptions);
            if (result) {
                result = imagePacker.addImage(pixelMap);
                if (result) {
                    long dataSize = imagePacker.finalizePacking();
                }
            }
            outputStream.flush();
            outputStream.close();
            valuesBucket.clear();
            //解除独占
            valuesBucket.putInteger("is_pending", 0);
            helper.update(uri, valuesBucket, null);
            return uri;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 设置拍照布局
     */
    private void setTakePhotoLayout(Component component) {
        mIvCameraCrop.setVisibility(Component.VISIBLE);
        surfaceContainer.setVisibility(Component.VISIBLE);
        mLlCameraOption.setVisibility(Component.VISIBLE);
        mCropImageView.setVisibility(Component.HIDE);
        mLlCameraResult.setVisibility(Component.HIDE);
        mViewCameraCropBottom.setText(getString(ResourceTable.String_touch_to_focus));
        cameraAbility = cameraKit.getCameraAbility(cameraId);
    }

    // 设置自动对焦
    private void setAutoFocus(Component component) {
        for (int i = 0; i < cameraAbility.getSupportedAfMode().length; i++) {
            if (cameraAbility.getSupportedAfMode()[i] == AfResult.State.AF_STATE_AUTO_FOCUSED) {
                framePreviewConfigBuilder.setAfMode(AfResult.State.AF_STATE_AUTO_FOCUSED, null);
                break;
            }
        }
    }

    /**
     * 设置裁剪布局
     */
    private void setCropLayout() {
        mIvCameraCrop.setVisibility(Component.HIDE);
        surfaceContainer.setVisibility(Component.HIDE);
        mLlCameraOption.setVisibility(Component.HIDE);
        mCropImageView.setVisibility(Component.VISIBLE);
        mLlCameraResult.setVisibility(Component.VISIBLE);
        mViewCameraCropBottom.setText("");
    }

    @Override
    public void onImageArrival(ImageReceiver imageReceiver) {
        ohos.media.image.Image image = imageReceiver.readLatestImage();
        if (image == null) {
            return;
        }
        ohos.media.image.Image.Component component = image.getComponent(ImageFormat.ComponentType.JPEG);
        byte[] bytes = new byte[component.remaining()];
        ByteBuffer buffer = component.getBuffer();
        buffer.get(bytes);
        //ImageSource imageSource = ImageSource.create(bytes, 50,  mIvCameraCrop.getEstimatedWidth(), new ImageSource.SourceOptions());
        ImageSource imageSource = ImageSource.create(bytes, new ImageSource.SourceOptions());
        ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
        PixelMap original = imageSource.createPixelmap(options);
        Rect cropRect = new Rect(
                offsetY,
                offsetX,
                mIvCameraCrop.getEstimatedWidth(),
                mIvCameraCrop.getEstimatedHeight());
        PixelMap.InitializationOptions options1 = new PixelMap.InitializationOptions();
        options1.size = new Size(cropRect.width, cropRect.height);
        PixelMap map = PixelMap.create(original, cropRect, options1);
        getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                //将手动裁剪区域设置成与扫描框一样大
            }
        });
        image.release();
        cropImage(map);
    }

    private class CameraStateCallbackImpl extends CameraStateCallback {
        CameraStateCallbackImpl() {
        }

        @Override
        public void onCreated(Camera camera) {
            if (surfaceProvider.getSurfaceOps().isPresent()) {
                previewSurface = surfaceProvider.getSurfaceOps().get().getSurface();
            }
            if (previewSurface == null) {
                HiLog.error(LABEL_LOG, "%{public}s", "Create camera filed, preview surface is null");
                return;
            }
            CameraConfig.Builder cameraConfigBuilder = camera.getCameraConfigBuilder();
            cameraConfigBuilder.addSurface(previewSurface);
            cameraConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
            camera.configure(cameraConfigBuilder.build());
            cameraDevice = camera;
        }

        @Override
        public void onConfigured(Camera camera) {
            framePreviewConfigBuilder = camera.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW);
            framePreviewConfigBuilder.addSurface(previewSurface);
            camera.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }
    }

    private class SurfaceCallBack implements SurfaceOps.Callback {
        @Override
        public void surfaceCreated(SurfaceOps callbackSurfaceOps) {
            if (callbackSurfaceOps != null) {
                callbackSurfaceOps.setFixedSize(SCREEN_HEIGHT, SCREEN_WIDTH);
            }
            eventHandler.postTask(CameraAbility.this::openCamera, 200);
        }

        @Override
        public void surfaceChanged(SurfaceOps callbackSurfaceOps, int format, int width, int height) {
        }

        @Override
        public void surfaceDestroyed(SurfaceOps callbackSurfaceOps) {
        }
    }

    private void showTips(Context context, String msg) {
        getUITaskDispatcher().asyncDispatch(() -> new ToastDialog(context).setText(msg).show());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (cameraDevice != null) {
            framePreviewConfigBuilder = null;
            try {
                cameraDevice.release();
                cameraDevice = null;
                surfaceProvider.clearFocus();
                surfaceProvider.removeFromWindow();
                surfaceProvider = null;
            } catch (Exception e) {

            }
        }
    }
}
