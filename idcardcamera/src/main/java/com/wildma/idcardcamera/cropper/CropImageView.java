package com.wildma.idcardcamera.cropper;


import com.wildma.idcardcamera.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * Author       wildma
 * Github       https://github.com/wildma
 * Date         2018/6/24
 * Desc	        ${裁剪布局}
 */
public class CropImageView extends StackLayout {

    private Image mImageView;
    private CropOverlayView mCropOverlayView;

    public CropImageView(Context context) {
        super(context);
    }

    public CropImageView(Context context, AttrSet attrs) {
        super(context, attrs);
        Component component = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_crop_image_view, null, false);
        mImageView = (Image) component.findComponentById(ResourceTable.Id_img_crop);
        mCropOverlayView = (CropOverlayView) component.findComponentById(ResourceTable.Id_overlay_crop);
        addComponent(component);
    }

    public void setImageBitmap(PixelMap bitmap) {
        if (mCropOverlayView != null) {
            mCropOverlayView.setBitmap(bitmap);
        }
        if (mImageView != null) {
            mImageView.setPixelMap(bitmap);
        }
    }

    public void crop(CropListener listener, boolean needStretch) {
        if (listener == null)
            return;
        mCropOverlayView.crop(listener, needStretch);
    }
}
