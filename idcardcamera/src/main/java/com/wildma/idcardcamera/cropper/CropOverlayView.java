package com.wildma.idcardcamera.cropper;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Arrays;

import static ohos.agp.render.Shader.TileMode.CLAMP_TILEMODE;

/**
 * Author       wildma
 * Github       https://github.com/wildma
 * Date         2018/6/24
 * Desc	        ${裁剪区域布局}
 */
public class CropOverlayView extends Image implements Component.DrawTask, Component.TouchEventListener, Component.EstimateSizeListener {

    private static final float EPSILON = 0.0000001f;
    private int defaultMargin = 100;
    private int minDistance = 100;
    private int vertexSize = 30;
    private int gridSize = 3;

    private Point topLeft = new Point(0, 0);
    private Point topRight = new Point(0, 0);
    private Point bottomLeft = new Point(0, 0);
    private Point bottomRight = new Point(0, 0);

    private float touchDownX, touchDownY;
    private CropPosition cropPosition;

    private int currentWidth = 0;
    private int currentHeight = 0;

    private int minX, maxX, minY, maxY;
    private PixelMap bitmap;

    public CropOverlayView(Context context) {
        super(context);
        setTouchEventListener(this);
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    public CropOverlayView(Context context, AttrSet attrs) {
        super(context, attrs);
        setTouchEventListener(this);
        //setEstimateSizeListener(this);
        addDrawTask(this);
    }

    public void setBitmap(PixelMap bitmap) {
        this.bitmap = bitmap;
        resetPoints();
        invalidate();
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        PixelMap emptyPixelMap = createEmptyPixelMap(pixelMap);
        super.setPixelMap(emptyPixelMap);
        this.bitmap = emptyPixelMap;
    }

    /**
     * Creates an empty pixel map
     *
     * @param pixelMap pixelMap
     * @return pixelMap
     */
    public PixelMap createEmptyPixelMap(PixelMap pixelMap) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        if (getEstimatedWidth() == 0 && getEstimatedHeight() == 0) {
            return null;
        }
        initializationOptions.size = new Size(getEstimatedWidth(), getEstimatedHeight());
        initializationOptions.alphaType = AlphaType.UNKNOWN;
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        return PixelMap.create(initializationOptions);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL_STYLE);

        if (getWidth() != currentWidth || getHeight() != currentHeight) {
            currentWidth = getWidth();
            currentHeight = getHeight();
            resetPoints();
        }
        drawBackground(canvas);
        drawVertex(canvas);
        drawEdge(canvas);
        //drawGrid(canvas);//裁剪框内部线条
    }

    private void resetPoints() {
        // 1. calculate bitmap size in new canvas
        float scaleX = bitmap.getImageInfo().size.width * 1.0f / getWidth();
        float scaleY = bitmap.getImageInfo().size.height * 1.0f / getHeight();
        float maxScale = Math.max(scaleX, scaleY);
        // 2. determine minX , maxX if maxScale = scaleY | minY, maxY if maxScale = scaleX
        int minX = 0;
        int maxX = getWidth();
        int minY = 0;
        int maxY = getHeight();
        if (equals(maxScale,scaleY)) { // image very tall
            int bitmapInCanvasWidth = (int) (bitmap.getImageInfo().size.width / maxScale);
            minX = (getWidth() - bitmapInCanvasWidth) / 2;
            maxX = getWidth() - minX;
        } else { // image very wide
            int bitmapInCanvasHeight = (int) (bitmap.getImageInfo().size.height / maxScale);
            minY = (getHeight() - bitmapInCanvasHeight) / 2;
            maxY = getHeight() - minY;
        }

        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;

        if (maxX - minX < defaultMargin || maxY - minY < defaultMargin)
            defaultMargin = 0; // remove min
        else
            defaultMargin = 30;
        topLeft = new Point(minX + defaultMargin, minY + defaultMargin);
        topRight = new Point(maxX - defaultMargin, minY + defaultMargin);
        bottomLeft = new Point(minX + defaultMargin, maxY - defaultMargin);
        bottomRight = new Point(maxX - defaultMargin, maxY - defaultMargin);
    }

    private boolean equals(final float one, final float two) {
        return (Math.abs(one - two) < EPSILON);
    }

    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int width = Component.EstimateSpec.getSize(widthMeasureSpec);
        int height = Component.EstimateSpec.getSize(heightMeasureSpec);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.UNCONSTRAINT),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.UNCONSTRAINT));
        return true;
    }

    private void drawBackground(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(new Color(Color.getIntColor("#66000000")));
        paint.setStyle(Paint.Style.FILL_STYLE);

        if (topLeft == null) {
            return;
        }

        Path path = new Path();
        path.moveTo(topLeft.getPointX(), topLeft.getPointY());
        path.lineTo(topRight.getPointX(), topRight.getPointY());
        path.lineTo(bottomRight.getPointX(), bottomRight.getPointY());
        path.lineTo(bottomLeft.getPointX(), bottomLeft.getPointY());
        path.close();

        canvas.save();
        canvas.clipPath(path, Canvas.ClipOp.DIFFERENCE);
        canvas.drawColor(Color.getIntColor("#66000000"), BlendMode.COLOR);
        canvas.restore();
    }

    private void drawVertex(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL_STYLE);

        canvas.drawCircle(topLeft.getPointX(), topLeft.getPointY(), vertexSize, paint);
        canvas.drawCircle(topRight.getPointX(), topRight.getPointY(), vertexSize, paint);
        canvas.drawCircle(bottomLeft.getPointX(), bottomLeft.getPointY(), vertexSize, paint);
        canvas.drawCircle(bottomRight.getPointX(), bottomRight.getPointY(), vertexSize, paint);

//        Log.e("stk",
//                "vertextPoints=" +
//                        topLeft.toString() + " " + topRight.toString() + " " + bottomRight.toString() + " " + bottomLeft.toString());

    }

    private void drawEdge(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);
        paint.setAntiAlias(true);

        canvas.drawLine(topLeft.getPointX(), topLeft.getPointY(), topRight.getPointX(), topRight.getPointY(), paint);
        canvas.drawLine(topLeft.getPointX(), topLeft.getPointY(), bottomLeft.getPointX(), bottomLeft.getPointY(), paint);
        canvas.drawLine(bottomRight.getPointX(), bottomRight.getPointY(), topRight.getPointX(), topRight.getPointY(), paint);
        canvas.drawLine(bottomRight.getPointX(), bottomRight.getPointY(), bottomLeft.getPointX(), bottomLeft.getPointY(), paint);
    }

    private void drawGrid(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(2);
        paint.setAntiAlias(true);

        for (int i = 1; i <= gridSize; i++) {
            int topDistanceX = (int) Math.abs(topLeft.getPointX() - topRight.getPointX()) / (gridSize + 1) * i;
            int topDistanceY = (int) Math.abs((topLeft.getPointY() - topRight.getPointY()) / (gridSize + 1) * i);

            Point top = new Point(
                    topLeft.getPointX() < topRight.getPointX() ? topLeft.getPointX() + topDistanceX : topLeft.getPointX() - topDistanceX,
                    topLeft.getPointY() < topRight.getPointY() ? topLeft.getPointY() + topDistanceY : topLeft.getPointY() - topDistanceY);

            int bottomDistanceX = (int) Math.abs((bottomLeft.getPointX() - bottomRight.getPointX()) / (gridSize + 1) * i);
            int bottomDistanceY = (int) Math.abs((bottomLeft.getPointY() - bottomRight.getPointY()) / (gridSize + 1) * i);
            Point bottom = new Point(
                    bottomLeft.getPointX() < bottomRight.getPointX() ? bottomLeft.getPointX() + bottomDistanceX : bottomLeft.getPointX() - bottomDistanceX,
                    bottomLeft.getPointY() < bottomRight.getPointY() ? bottomLeft.getPointY() + bottomDistanceY : bottomLeft.getPointY() - bottomDistanceY);

            canvas.drawLine(top.getPointX(), top.getPointY(), bottom.getPointX(), bottom.getPointY(), paint);

            int leftDistanceX = (int) Math.abs((topLeft.getPointX() - bottomLeft.getPointX()) / (gridSize + 1) * i);
            int leftDistanceY = (int) Math.abs((topLeft.getPointY() - bottomLeft.getPointY()) / (gridSize + 1) * i);

            Point left = new Point(
                    topLeft.getPointX() < bottomLeft.getPointX() ? topLeft.getPointX() + leftDistanceX : topLeft.getPointX() - leftDistanceX,
                    topLeft.getPointY() < bottomLeft.getPointY() ? topLeft.getPointY() + leftDistanceY : topLeft.getPointY() - leftDistanceY);

            int rightDistanceX = (int) Math.abs((topRight.getPointX() - bottomRight.getPointX()) / (gridSize + 1) * i);
            int rightDistanceY = (int) Math.abs((topRight.getPointY() - bottomRight.getPointY()) / (gridSize + 1) * i);

            Point right = new Point(
                    topRight.getPointX() < bottomRight.getPointX() ? topRight.getPointX() + rightDistanceX : topRight.getPointX() - rightDistanceX,
                    topRight.getPointY() < bottomRight.getPointY() ? topRight.getPointY() + rightDistanceY : topRight.getPointY() - rightDistanceY);

            canvas.drawLine(left.getPointX(), left.getPointY(), right.getPointX(), right.getPointY(), paint);
        }

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
                invalidate();
                break;
            case TouchEvent.PRIMARY_POINT_DOWN:
                invalidate();
                onActionDown(event);
                return true;
            case TouchEvent.POINT_MOVE:
                invalidate();
                onActionMove(event);
                return true;
        }
        return false;
    }

    private void onActionDown(TouchEvent event) {
        MmiPoint mmiPoint = event.getPointerPosition(0);
        touchDownX = mmiPoint.getX();
        touchDownY = mmiPoint.getY();
        Point touchPoint = new Point((int) touchDownX, touchDownY);
        int minDistance = distance(touchPoint, topLeft);
        cropPosition = CropPosition.TOP_LEFT;
        if (minDistance > distance(touchPoint, topRight)) {
            minDistance = distance(touchPoint, topRight);
            cropPosition = CropPosition.TOP_RIGHT;
        }
        if (minDistance > distance(touchPoint, bottomLeft)) {
            minDistance = distance(touchPoint, bottomLeft);
            cropPosition = CropPosition.BOTTOM_LEFT;
        }
        if (minDistance > distance(touchPoint, bottomRight)) {
            minDistance = distance(touchPoint, bottomRight);
            cropPosition = CropPosition.BOTTOM_RIGHT;
        }
    }

    private int distance(Point src, Point dst) {
        return (int) Math.sqrt(Math.pow(src.getPointX() - dst.getPointX(), 2) + Math.pow(src.getPointY() - dst.getPointY(), 2));
    }

    private void onActionMove(TouchEvent event) {
        MmiPoint mmiPoint = event.getPointerPosition(0);
        int deltaX = (int) (mmiPoint.getX() - touchDownX);
        int deltaY = (int) (mmiPoint.getY() - touchDownY);

        switch (cropPosition) {
            case TOP_LEFT:
                adjustTopLeft(deltaX, deltaY);
                invalidate();
                break;
            case TOP_RIGHT:
                adjustTopRight(deltaX, deltaY);
                invalidate();
                break;
            case BOTTOM_LEFT:
                adjustBottomLeft(deltaX, deltaY);
                invalidate();
                break;
            case BOTTOM_RIGHT:
                adjustBottomRight(deltaX, deltaY);
                invalidate();
                break;
        }
        touchDownX = mmiPoint.getX();
        touchDownY = mmiPoint.getY();
    }

    private void adjustTopLeft(int deltaX, int deltaY) {
        int newX = (int) topLeft.getPointX() + deltaX;
        if (newX < minX) newX = minX;
        if (newX > maxX) newX = maxX;

        int newY = (int) topLeft.getPointY() + deltaY;
        if (newY < minY) newY = minY;
        if (newY > maxY) newY = maxY;

        topLeft.modify(newX, newY);
    }

    private void adjustTopRight(int deltaX, int deltaY) {
        int newX = (int) topRight.getPointX() + deltaX;
        if (newX > maxX) newX = maxX;
        if (newX < minX) newX = minX;

        int newY = (int) topRight.getPointY() + deltaY;
        if (newY < minY) newY = minY;
        if (newY > maxY) newY = maxY;

        topRight.modify(newX, newY);
    }

    private void adjustBottomLeft(int deltaX, int deltaY) {
        int newX = (int) bottomLeft.getPointX() + deltaX;
        if (newX < minX) newX = minX;
        if (newX > maxX) newX = maxX;

        int newY = (int) bottomLeft.getPointY() + deltaY;
        if (newY > maxY) newY = maxY;
        if (newY < minY) newY = minY;

        bottomLeft.modify(newX, newY);
    }

    private void adjustBottomRight(int deltaX, int deltaY) {
        int newX = (int) bottomRight.getPointX() + deltaX;
        if (newX > maxX) newX = maxX;
        if (newX < minX) newX = minX;

        int newY = (int) bottomRight.getPointY() + deltaY;
        if (newY > maxY) newY = maxY;
        if (newY < minY) newY = minY;

        bottomRight.modify(newX, newY);
    }

    public void crop(CropListener cropListener, boolean needStretch) {
        if (topLeft == null) return;

        // calculate bitmap size in new canvas
        float scaleX = bitmap.getImageInfo().size.width * 1.0f / getWidth();
        float scaleY = bitmap.getImageInfo().size.height * 1.0f / getHeight();
        float maxScale = Math.max(scaleX, scaleY);
        // re-calculate coordinate in original bitmap
        //Log.e("stk", "maxScale=" + maxScale);

        Point bitmapTopLeft = new Point((int) ((topLeft.getPointX() - minX) * maxScale), (int) ((topLeft.getPointY() - minY) * maxScale));
        Point bitmapTopRight = new Point((int) ((topRight.getPointX() - minX) * maxScale), (int) ((topRight.getPointY() - minY) * maxScale));
        Point bitmapBottomLeft = new Point((int) ((bottomLeft.getPointX() - minX) * maxScale), (int) ((bottomLeft.getPointY() - minY) * maxScale));
        Point bitmapBottomRight = new Point((int) ((bottomRight.getPointX() - minX) * maxScale), (int) ((bottomRight.getPointY() - minY) * maxScale));

        //Log.e("stk", "bitmapPoints="
//                + bitmapTopLeft.toString() + " "
//                + bitmapTopRight.toString() + " "
//                + bitmapBottomRight.toString() + " "
//                + bitmapBottomLeft.toString() + " ");

        PixelMap.InitializationOptions outputOptions = new PixelMap.InitializationOptions();
        outputOptions.size = new Size(bitmap.getImageInfo().size.width + 1, bitmap.getImageInfo().size.height + 1);
        outputOptions.pixelFormat = PixelFormat.ARGB_8888;
        outputOptions.editable = true;
        PixelMap output = PixelMap.create(outputOptions);
        Canvas canvas = new Canvas(new Texture(output));
        Paint paint = new Paint();
        // 1. draw path
        Path path = new Path();
        path.moveTo(bitmapTopLeft.getPointX(), bitmapTopLeft.getPointY());
        path.lineTo(bitmapTopRight.getPointX(), bitmapTopRight.getPointY());
        path.lineTo(bitmapBottomRight.getPointX(), bitmapBottomRight.getPointY());
        path.lineTo(bitmapBottomLeft.getPointX(), bitmapBottomLeft.getPointY());
        path.close();
        canvas.drawPath(path, paint);

        // 2. draw original bitmap
        PixelMapHolder originalHolder = new PixelMapHolder(bitmap);
        Shader originalShader = new PixelMapShader(originalHolder, CLAMP_TILEMODE, CLAMP_TILEMODE);
        paint.setShader(originalShader, Paint.ShaderType.PIXELMAP_SHADER);
        paint.setBlendMode(BlendMode.SRC_IN);
        canvas.drawPaint(paint);
        //cropListener.onFinish(output);
        //3. cut
        ohos.agp.utils.Rect cropRect = new ohos.agp.utils.Rect(
                (int) Math.min(bitmapTopLeft.getPointX(), bitmapBottomLeft.getPointX()),
                (int) Math.min(bitmapTopLeft.getPointY(), bitmapTopRight.getPointY()),
                (int) Math.max(bitmapBottomRight.getPointX(), bitmapTopRight.getPointX()),
                (int) Math.max(bitmapBottomRight.getPointY(), bitmapBottomLeft.getPointY()));

        if (cropRect.getWidth() <= 0 || cropRect.getHeight() <= 0) { //用户裁剪的宽或高为0
            cropListener.onFinish(null);
            return;
        }
        ohos.media.image.common.Rect cutRect = new ohos.media.image.common.Rect(
                cropRect.left,
                cropRect.top,
                cropRect.getWidth(),
                cropRect.getHeight()
        );
        PixelMap.InitializationOptions cutOptions = new PixelMap.InitializationOptions();
        cutOptions.size = new Size(cropRect.getWidth(), cropRect.getHeight());
        cutOptions.pixelFormat = PixelFormat.ARGB_8888;
        cutOptions.editable = true;
        PixelMap cut = PixelMap.create(output, cutRect, cutOptions);
        //cropListener.onFinish(cut);
        needStretch = true;
        if (!needStretch) {
            cropListener.onFinish(cut);
        } else {
            // 4. re-calculate coordinate in cropRect
            float xTL = bitmapTopLeft.getPointX() > bitmapBottomLeft.getPointX() ? bitmapTopLeft.getPointX() - bitmapBottomLeft.getPointX() : 0;
            float yTL = bitmapTopLeft.getPointY() > bitmapTopRight.getPointY() ? bitmapTopLeft.getPointY() - bitmapTopRight.getPointY() : 0;
            Point cutTopLeft = new Point(xTL, yTL);

            float xTR = bitmapTopRight.getPointX() > bitmapBottomRight.getPointX() ? cropRect.getWidth() : cropRect.getWidth() - Math.abs(bitmapBottomRight.getPointX() - bitmapTopRight.getPointX());
            float yTR = bitmapTopLeft.getPointY() > bitmapTopRight.getPointY() ? 0 : Math.abs(bitmapTopLeft.getPointY() - bitmapTopRight.getPointY());
            Point cutTopRight = new Point(xTR, yTR);

            float xBL = bitmapTopLeft.getPointX() > bitmapBottomLeft.getPointX() ? 0 : Math.abs(bitmapTopLeft.getPointX() - bitmapBottomLeft.getPointX());
            float yBL = bitmapBottomLeft.getPointY() > bitmapBottomRight.getPointY() ? cropRect.getHeight() : cropRect.getHeight() - Math.abs(bitmapBottomRight.getPointY() - bitmapBottomLeft.getPointY());
            Point cutBottomLeft = new Point(xBL, yBL);

            float xBR = bitmapTopRight.getPointX() > bitmapBottomRight.getPointX() ? cropRect.getWidth() - Math.abs(bitmapBottomRight.getPointX() - bitmapTopRight.getPointX()) : cropRect.getWidth();
            float yBR = bitmapBottomLeft.getPointY() > bitmapBottomRight.getPointY() ? cropRect.getHeight() - Math.abs(bitmapBottomRight.getPointY() - bitmapBottomLeft.getPointY()) : cropRect.getHeight();
            Point cutBottomRight = new Point(xBR, yBR);
/*
            Log.e("stk", cut.getEstimatedWidth() + "x" + cut.getEstimatedHeight());

            Log.e("stk", "cutPoints="
                    + cutTopLeft.toString() + " "
                    + cutTopRight.toString() + " "
                    + cutBottomRight.toString() + " "
                    + cutBottomLeft.toString() + " ");
*/

            float width = cut.getImageInfo().size.width;
            float height = cut.getImageInfo().size.height;

            float[] src = new float[]{cutTopLeft.getPointX(), cutTopLeft.getPointY(), cutTopRight.getPointX(), cutTopRight.getPointY(), cutBottomRight.getPointX(), cutBottomRight.getPointY(), cutBottomLeft.getPointX(), cutBottomLeft.getPointY()};
            float[] dst = new float[]{0, 0, width, 0, width, height, 0, height};

            Matrix matrix = new Matrix();
            matrix.setPolyToPoly(src, 0, dst, 0, 4);
            PixelMap.InitializationOptions stretchOptions = new PixelMap.InitializationOptions();
            stretchOptions.size = new Size(cut.getImageInfo().size.width, cut.getImageInfo().size.height);
            stretchOptions.pixelFormat = PixelFormat.ARGB_8888;
            PixelMap stretch = PixelMap.create(stretchOptions);
            Canvas stretchCanvas = new Canvas(new Texture(stretch));

            PixelMapHolder stretchHolder = new PixelMapHolder(cut);
            Shader stretchShader = new PixelMapShader(stretchHolder, CLAMP_TILEMODE, CLAMP_TILEMODE);
            Paint stretchPaint = new Paint();
            stretchPaint.setShader(stretchShader, Paint.ShaderType.PIXELMAP_SHADER);
            //stretchCanvas.concat(matrix);
            stretchCanvas.drawPaint(stretchPaint);
            cropListener.onFinish(stretch);
        }
    }

    private int WIDTH_BLOCK = 40;
    private int HEIGHT_BLOCK = 40;

    private float[] generateVertices(int widthBitmap, int heightBitmap) {

        float[] vertices = new float[(WIDTH_BLOCK + 1) * (HEIGHT_BLOCK + 1) * 2];

        float widthBlock = (float) widthBitmap / WIDTH_BLOCK;
        float heightBlock = (float) heightBitmap / HEIGHT_BLOCK;

        for (int i = 0; i <= HEIGHT_BLOCK; i++)
            for (int j = 0; j <= WIDTH_BLOCK; j++) {
                vertices[i * ((HEIGHT_BLOCK + 1) * 2) + (j * 2)] = j * widthBlock;
                vertices[i * ((HEIGHT_BLOCK + 1) * 2) + (j * 2) + 1] = i * heightBlock;
            }
        return vertices;
    }
}
