package com.wildma.idcardcamera.utils;

import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

import java.util.Arrays;

/**
 * Author       wildma
 * Github       https://github.com/wildma
 * Date         2018/6/10
 * Desc	        ${权限工具类}
 */

public class PermissionUtils {

    public static void requestPermissions(Context context) {
        String[] permissions = {
                SystemPermission.WRITE_USER_STORAGE, SystemPermission.READ_USER_STORAGE, SystemPermission.CAMERA,
                SystemPermission.MICROPHONE, SystemPermission.LOCATION
        };
        context.requestPermissionsFromUser(Arrays.stream(permissions)
                .filter(permission -> context.verifySelfPermission(permission) != IBundleManager.PERMISSION_GRANTED).toArray(String[]::new), 0);
    }
}