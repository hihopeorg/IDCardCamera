package com.wildma.idcardcamera.utils;


import ohos.aafwk.ability.DataAbilityHelper;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageFormat;
import ohos.media.image.common.Rect;

import java.io.*;

/**
 * Author   wildma
 * Github   https://github.com/wildma
 * Date     2018/6/24
 * Desc     ${图片相关工具类}
 */

public class ImageUtils {

    /**
     * 保存图片
     *
     * @param src      源图片
     * @param filePath 要保存到的文件路径
     * @param format   格式
     * @return {@code true}: 成功<br>{@code false}: 失败
     */
    public static boolean save(PixelMap src, String filePath, ImageFormat format) {
        return save(src, FileUtils.getFileByPath(filePath), format, false);
    }

    /**
     * 保存图片
     *
     * @param src    源图片
     * @param file   要保存到的文件
     * @param format 格式
     * @return {@code true}: 成功<br>{@code false}: 失败
     */
    public static boolean save(PixelMap src, File file, ImageFormat format) {
        return save(src, file, format, false);
    }

    /**
     * 保存图片
     *
     * @param src      源图片
     * @param filePath 要保存到的文件路径
     * @param format   格式
     * @param recycle  是否回收
     * @return {@code true}: 成功<br>{@code false}: 失败
     */
    public static boolean save(PixelMap src, String filePath, ImageFormat format, boolean recycle) {
        return save(src, FileUtils.getFileByPath(filePath), format, recycle);
    }

    /**
     * 保存图片
     *
     * @param pixelMap     源图片
     * @param file    要保存到的文件
     * @param format  格式
     * @param recycle 是否回收
     * @return {@code true}: 成功<br>{@code false}: 失败
     */
    public static boolean save(PixelMap pixelMap, File file, ImageFormat format, boolean recycle) {
        if (isEmptyBitmap(pixelMap) || !FileUtils.createOrExistsFile(file)) {
            return false;
        }
        System.out.println(pixelMap.getImageInfo().size.width + ", " + pixelMap.getImageInfo().size.height);
        ImagePacker imagePacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.format = "image/jpeg";
        packingOptions.quality = 100;
        OutputStream os = null;
        boolean ret = false;
        try {
            os = new BufferedOutputStream(new FileOutputStream(file));
            ret = imagePacker.initializePacking(os, packingOptions);
            if (ret) {
                ret = imagePacker.addImage(pixelMap);
                if (ret) {
                    long dataSize = imagePacker.finalizePacking();
                }
            }
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            FileUtils.closeIO(os);
        }
        return ret;
    }

    /**
     * 判断bitmap对象是否为空
     *
     * @param src 源图片
     * @return {@code true}: 是<br>{@code false}: 否
     */
    private static boolean isEmptyBitmap(PixelMap src) {
        return src == null || src.getImageInfo().size.width == 0 || src.getImageInfo().size.height == 0;
    }

    public static PixelMap getPixelMapFromByte(byte[] bytes, int width, int height) {
        return null;
    }
}
