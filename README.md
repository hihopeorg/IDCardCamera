# IDCardCamera

**本项目是基于开源项目IDCardCamera进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/wildma/IDCardCamera ）追踪到原项目版本**

#### 项目介绍

- 项目名称：IDCardCamera
- 所属系列：ohos的第三方组件适配移植
- 功能：IDCardCamera自定义身份证相机功能。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/wildma/IDCardCamera
- 原项目基线版本：v1.1.1
- 编程语言：Java
- 外部库依赖：无

#### 展示效果

![avatar](screenshot/preview.gif)

#### 安装教程

##### 方案一：

1. 编译组件har包IDCardCamera.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二:

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'ss.anoop.ohos:IDCardCamera:1.0.0'
 }
```

#### 使用说明
```
 /**
     * 身份证正面
     */
    public void front(Component component) {
        IDCardCamera.create(this).openCamera(IDCardCamera.TYPE_IDCARD_FRONT);
    }

    /**
     * 身份证反面
     */
    public void back(Component component) {
        IDCardCamera.create(this).openCamera(IDCardCamera.TYPE_IDCARD_BACK);
    }
```

 在 onAbilityResult 方法中获取裁剪后的图片
```
    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultCode == IDCardCamera.RESULT_CODE) {
            //获取图片路径，显示图片
            final String path = IDCardCamera.getImagePath(resultData);
            if (!TextUtils.isEmpty(path)) {
                PixelMap pixelMap = decodeFile(path);
                if (requestCode == IDCardCamera.TYPE_IDCARD_FRONT) { //身份证正面
                    mIvFront.setPixelMap(pixelMap);
                } else if (requestCode == IDCardCamera.TYPE_IDCARD_BACK) {  //身份证反面
                    mIvBack.setPixelMap(pixelMap);
                }
            }
        }
    }
```

#### 版本迭代

- v1.0.0

IDCardCamera ohos 化组件对标的功能实现。

具体功能如下：

- 自定义相机界面
- 支持开启闪光灯
- 支持手动触摸屏幕对焦
- 支持自动对焦
- 支持图片手动不规则裁剪

不支持：
- 图片自动裁剪（drawBitmapMesh函数不支持）
- 清理缓存（不支持删除）


#### 版权和许可信息

```
   Copyright 2018 wildma

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```


